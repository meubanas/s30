const express = require('express');
const mongoose = require('mongoose');

const port = 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Syntax:
	// mongoose.connect('<connection string>, {middlewares}')

	mongoose.connect('mongodb+srv://admin:admin131@zuitt.cfuxf.mongodb.net/session30?retryWrites=true&w=majority', 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);

let db = mongoose.connection;

	// console.error.bind(console)- print errors in the browser and in the terminal
	db.on("error", console.error.bind(console, "Connection Error"))

	// if the connection is succesful, this will be the output in our console.
	db.once('open', () => console.log('Connected to the cloud database'))

// Mongoose Schema

// const taskSchema = new mongoose.Schema({

// 	// define the name of our schema - taskSchema
// 	// new mongoose.Schema method - to make a schema
// 	// we will be needing the name of the task and its status
// 	// each field will require a data type

// 	name: String,
// 	status: {
// 		type: String,
// 		default: 'pending'
// 	}
// });

// const Task = mongoose.model('Task', taskSchema);
// 	// (Task-first parameter) models use schemas and they act as middleman from the server to our database.
// 	// Model can now beused to run commands for interacting with our database
// 	// naming convention - Name of model should be capitalized and singular form- 'Task'
// 	// second parameter(taskSchema) is used to specify the schema of the documents that be restored in the mongoDB collection.

// // Business Logic
// 	/*
// 		1. Add a functionality to check if there are duplicate tasks
// 			-if the task already exists in the db, we return an error
// 			- if the task does'nt exists in the db, we add it in the db.
// 		2. The task data will be coming from the request body.
// 		3. Create a new Task object with name field property.

// 	*/

// app.post('/tasks', (req, res) => {

// 	// check any duplicate task
// 	// err shorthand of error
// 	Task.findOne({name: req.body.name}, (err, result) => {

// 		// if there are matches,
// 		if(result != null && result.name === req.body.name){

// 			// will return this message
// 			return res.send('Duplicate task found.')

// 		} else {

// 			// if no matches it will create new Task
// 			let newTask = new Task({

// 				name: req.body.name,
// 				rank: req.body.rank
// 			})

// 			// save method will accept a callback function which stores andy errors in the first parameter and will store the newly saved documents in the second parameter
// 			newTask.save((saveErr, savedTask) => {

// 				//  if there are any erros,it will print the error.
// 				if(saveErr){

// 					return console.error(saveErr)

// 				// no error found, return the status code the message.
// 				} else {

// 					return res.status(201).send('New Task Created')
// 				}
// 			})
// 		}
// 	})
// })

// // Retirieving all tasks

// app.get('/tasks', (req, res) => {

// 	Task.find({}, (err, result) => {

// 		if(err){

// 			return console.log(err);
// 		} else {

// 			return res.status(200).json({
// 				data: result
// 			})
// 		}
// 	})
// })

// Mini Activity

const userSchema = new mongoose.Schema({

	// define the name of our schema - taskSchema
	// new mongoose.Schema method - to make a schema
	// we will be needing the name of the task and its status
	// each field will require a data type

	username: {
		type: String,
		default: 'pending'
	},
	password: {
		type: String,
		default: 'pending'
	}
});

const Task1 = mongoose.model('Task1', userSchema);


app.post('/login', (req, res) => {

	Task1.findOne({username: req.body.username}, (err, result) => {

		if(result != null && result.username === req.body.username){

			return res.send('Username was use already taken.')

		} else {

			let newTask = new Task1({

				username: req.body.username,
				password: req.body.password
			})

			newTask.save((saveErr, savedTask) => {

				if(saveErr){

					return console.error(saveErr)

				} else {

					return res.status(201).send('User account successfully created')
				}
			})
		}
	})
})

app.get('/login', (req, res) => {

	Task1.find({}, (err, result) => {

		if(err){

			return console.log(err);
		} else {

			return res.status(200).json({
				data: result
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at port ${port}`))